<!-- _Use this merge request description template when suggesting the removal of a sub-value from the GitLab values page._ -->

In order to remove a sub-value from the [GitLab values](https://about.gitlab.com/handbook/values/) page, please review the [process for removing sub-values}(https://about.gitlab.com/handbook/values/index.html#process-for-removing-sub-values) in the handbook. 

Please indicate the reasons for removing this sub-value (note: a sub-value must meet at least one of these criteria to be removed):

* [ ] This sub-value is a duplicate of an existing sub-value.
* [ ] This sub-value is not actionable
* [ ] This sub-value is vague and is too open to interpretation
* [ ] This sub-value can be combined with other sub-values to create one sub-value
* [ ] Other

If other, please add an explanation: 

<!-- Please do not edit the information below -->
/assign @sytses @streas 

